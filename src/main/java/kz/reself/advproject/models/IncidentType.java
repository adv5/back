package kz.reself.advproject.models;

public enum IncidentType {
    FIRE,
    ROAD_ACCIDENT,
    WATER_ACCIDENT;

}
